---
title: "Sorvete de Abacate"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Ingredientes
2 xícaras de abacate amassado
2 colheres de sopa de suco de limão
1 lata de leite condensado de soja
2 colheres de sopa de açúcar mascavo
2 colheres de sopa xarope de bordo (maple syrup)

Modo de preparo
Bata todos os ingredientes no liquidificador até formarem um creme. Deixe no freezer por quatro horas e sirva.