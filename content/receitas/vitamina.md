---
title: "Vitamina de Abacate"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Ingredientes
½ abacate médio maduro;
1 ½ copo de leite de aveia ou outro de sua preferência;
2 colheres de açúcar mascavo (opcional)
Açúcar mascavo: benefícios e cuidados ao consumir

Modo de preparo
Bata tudo no liquidificador até ficar homogêneo e sirva.