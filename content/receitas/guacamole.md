---
title: "Guacamole"
date: 2021-10-24T10:54:33-03:00
draft: false
---

Ingredientes
1 abacate médio maduro;
1 dente de alho amassado;
½ cebola ou cebola roxa picada;
Suco de 1 limão;
1 tomate picado;
1 colher de azeite de oliva;
Sal e pimenta do reino a gosto.

Modo de preparo
Se quiser, coloque a cebola de molho dentro de um recipiente quente por cerca de dez minutos para reduzir a acidez. Também é possível usá-la diretamente. Em seguida, corte todos os ingredientes em pequenos pedaços e os amasse junto com a polpa do abacate em uma vasilha grande, temperando de acordo com seu gosto. Depois, você pode colocar a mistura em um recipiente fechado e levar à geladeira por mais ou menos uma hora, se quiser comer o guacamole geladinho. Mas você também pode comer assim que preparar. Aproveite!

